<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminInfoumumController extends \crocodicstudio\crudbooster\controllers\CBController {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
		public function cbInit() {
			$this->title_field = "nama";
			$this->limit = "1";
			$this->global_privilege = true;
			$this->button_table_action = false;
			$this->button_bulk_action = false;
			$this->show_numbering = FALSE;
			$this->button_add = false;
			$this->button_edit = false;
			$this->button_delete = false;
			$this->button_detail = FALSE;
			$this->button_show = false;
			$this->button_filter = false;
			$this->button_import = false;
			$this->button_export = false;
			$this->table         = 'infoumum';



			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ["label"=>"Kolom1","name"=>"kolom1","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
			//$this->form[] = ["label"=>"Kolom2","name"=>"kolom2","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          
	        $this->table_row_color[] = ["condition"=>"[id] == '1'","color"=>"success"];
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();
	        $this->index_statistic[] = ['label'=>'Total ANTRIAN','count'=>DB::table('antrian')->where('id_statuspermohonan','=', null)->where('id_persetujuan','=', 1)->count(),'icon'=>'fa fa-home fa-3','color'=>'red','link'=>'admin/aju_antrian'];
	        $this->index_statistic[] = ['label'=>'Total PERPANJANGAN','count'=>DB::table('perpanjangan')->where('id_statuspermohonan','=', null)->where('id_persetujuan','=', 1)->count(),'icon'=>'fa fa-sliders','color'=>'yellow','link'=>'admin/perpanjangan'];
	        $this->index_statistic[] = ['label'=>'Antrian RAWAMANGUN','count'=>DB::table('antrian')->where('komplek_id','=', 1)->where('id_statuspermohonan','=', null)->where('id_persetujuan','=', 1)->count(),'icon'=>'fa fa-home','color'=>'blue','link'=>'admin/antrian_rawamangun'];
	        $this->index_statistic[] = ['label'=>'Antrian PONDOK BAMBU','count'=>DB::table('antrian')->where('komplek_id','=', 2)->where('id_statuspermohonan','=', null)->where('id_persetujuan','=', 1)->count(),'icon'=>'fa fa-home','color'=>'green','link'=>'admin/antrian_pondokbambu'];	
	        $this->index_statistic[] = ['label'=>'Antrian CIREUNDEU','count'=>DB::table('antrian')->where('komplek_id','=', 3)->where('id_statuspermohonan','=', null)->where('id_persetujuan','=', 1)->count(),'icon'=>'fa fa-home','color'=>'olive','link'=>'admin/antrian_cireundeu'];
	        $this->index_statistic[] = ['label'=>'Antrian PASAR MINGGU','count'=>DB::table('antrian')->where('komplek_id','=', 4)->where('id_statuspermohonan','=', null)->where('id_persetujuan','=', 1)->count(),'icon'=>'fa fa-home','color'=>'purple','link'=>'admin/antrian_pasarminggu'];
	        $this->index_statistic[] = ['label'=>'Antrian SLIPI','count'=>DB::table('antrian')->where('komplek_id','=', 5)->where('id_statuspermohonan','=', null)->where('id_persetujuan','=', 1)->count(),'icon'=>'fa fa-home','color'=>'teal','link'=>'admin/antrian_slipi'];
	        $this->index_statistic[] = ['label'=>'Antrian DIBLOKIR','count'=>DB::table('antrian')->where('id_statuspermohonan','=', null)->where('id_persetujuan','=', 4)->count(),'icon'=>'fa fa-server','color'=>'maroon','link'=>'admin/antrian_BLOKIR'];


	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = 
	        	"
	        	</br>
	        	<style>
				p.tinggi {
    				line-height: 200%;
				}
				p.rendah {
    				line-height: 150%;
				}
				</style>
	        	<div class='row'>
  				<div class='col-md-8'>
	        	<div class='panel panel-success'>
	        		<div class='panel-heading'><b>PROSEDUR PENGAJUAN ANTRIAN DAN PERPANJANGAN RUMAH NEGARA</b></div>
	        			<div class='panel-body'>

				<ol>
					<li style=‘text-align: justify;’>
						<p class='tinggi'>Akses form dengan klik menu&nbsp;<strong><font size=3><span class='label label-primary'>PENGAJUAN</span> - <span class='label label-primary'>PENGAJUAN ANTRIAN/PERPANJANGAN</span> - <span class='label label-primary'>ADD DATA</span></strong></font></p>
					</li>
					<li style=‘text-align: justify;’>
						<p class='tinggi'>Isi form yang tersedia dan scan dokumen pelengkap :</p>
						<ul>
							<p class='rendah'><li><em>Scan Surat Permohonan ber-Materai, </em></li></p>
							<p class='rendah'><li><em>Scan Kartu Keluarga (ANTRIAN) / Scan SKEP SIP Sebelumnya (PERPANJANGAN)</em></li></p>
							<p class='rendah'><li><em>Scan SKEP Pangkat Terakhir.</em></li></p>
							<p class='rendah'><li><strong>NB : hanya format PDF yang diterima oleh sistem, ukuran maksimal 2 MB</strong></li></p>
						</ul>
					</li>
					<li style=‘text-align: justify;’>
						<p class='tinggi'>Scan Surat Permohonan dapat diunduh di kolom <strong>DOWNLOAD FILE</strong></p>
					</li>
					<li style=‘text-align: justify;’>
						<p class='tinggi'>Setelah data disimpan, data Saudara akan muncul di tabel dengan status<strong>&nbsp <font size=3><span class='label label-warning'>MENUNGGU REVIEW</span></font></strong>&nbsp;<em>(Permohonan masih harus direview dan disetujui oleh Pengelola Data Rumah Negara dan Tidak Langsung masuk antrian / Diproses perpanjangannya)</em></p>
					</li>
					<li style=‘text-align: justify;’>
								<p class='tinggi'>Dokumen fisik bermaterai dikirimkan ke Subbag Kesejahteraan dengan alamat :</br></p>
								<ul>
									<center><div class='alert alert-danger'><strong>Subbagian Kesejahteraan, Gedung Papua Lt. 2
									</br>KANTOR PUSAT DIREKTORAT JENDERAL BEA DAN CUKAI
									</br>JL. A YANI BYPASS, JAKARTA TIMUR 13230</strong></div></center>
								</ul>
					</li>
					<li style=‘text-align: justify;’>
						<p class='tinggi'>Untuk mengetahui status review pengajuan antrian / perpanjangan, Saudara dapat mengakses menu <font size=3><span class='label label-primary'><strong>PENGAJUAN ANTRIAN / PERPANJANGAN</strong></span></font> dan mencari nama Saudara pada kolom search diatas kanan tabel</p>
					</li>
					<li style=‘text-align: justify;’>
						<p class='tinggi'><span style=‘text-decoration: underline;’><strong>Untuk Pengajuan ANTRIAN :</strong></span></p>
						<ul>
							<li>
								<p class='tinggi'>Jika status berubah menjadi&nbsp;<strong>&nbsp <font size=3><span class='label label-success'>SETUJU DIPROSES</span></font></strong> , proses selanjutnya adalah memantau nomor urut antrian di menu&nbsp;<strong><font size=3><span class='label label-primary'>ANTRIAN PER KOMPLEK</span> - <span class='label label-primary'>Antrian &nbsp;RAWAMANGUN/ PONDOK BAMBU/ dst. </span></strong></font></p>
							</li>
							<li>
								<p class='tinggi'>Jika status berubah menjadi&nbsp;<span style=‘color: #00ff00;’><strong><strong>&nbsp <font size=3><span class='label label-danger'>DITOLAK</span></font></strong></span></strong></span> , artinya permohonan Saudara tidak lengkap. Alasan penolakan dapat diakses pada menu&nbsp;<strong><font size=3><span class='label label-primary'>STATUS ANTRIAN </span> - <span class='label label-primary'>ANTRIAN DITOLAK. </span></strong></font></p>
							</li>
							<li>
								<p class='tinggi'>Untuk mengetahui nomor urut antrian Saudara, dapat dilihat melakukan search manual <em>(CTRL + F)</em> lalu ketikkan nama Saudara <strong>(penggunaan kolom search diatas tabel tidak menunjukkan antrian secara keseluruhan)</strong></p>
							</li>
							<li style=‘text-align: justify;’>
								<p class='tinggi'>NB : Dalam prosesnya, jika ada antrian yang sudah memenuhi syarat untuk diproses (naik ke-antrian pertama) menolak untuk diproses dengan alasan yang dapat dibenarkan (misal : masih menunggu tahun depan/ menunggu skep mutasi) status pengajuan akan diubah menjadi <strong>&nbsp <font size=3><span class='label label-danger'>MENUNGGU REVIEW</span></font></strong>Untuk membuka blokir tersebut Saudara dapat menghubungi Subbagian Kesejahteraan.</p>
							</li>
						</ul>
					</li>
					<li style=‘text-align: justify;’>
						<p class='tinggi'><span style=‘text-decoration: underline;’><strong>Untuk Pengajuan PERPANJANGAN :</strong></span></p>
						<ul>
							<li>
								<p class='tinggi'>Jika status berubah menjadi&nbsp;<strong>&nbsp <font size=3><span class='label label-success'>SETUJU DIPROSES</span></font></strong> , maka permohonan akan diproses dan lanjut ke prosedur ke-9<strong>.</strong></p>
							</li>
							<li>
								<p class='tinggi'>Jika status berubah menjadi&nbsp;<span style=‘color: #00ff00;’><strong><strong>&nbsp <font size=3><span class='label label-danger'>DITOLAK</span></font></strong></span></strong></span> , artinya permohonan Saudara tidak lengkap. Alasan penolakan dapat diakses pada menu&nbsp;<strong><font size=3><span class='label label-primary'>STATUS PERPANJANGAN </span> - <span class='label label-primary'> PERPANJANGAN DITOLAK. </span></strong></font></p>
							</li>
						</ul>
					</li>
					<li style=‘text-align: justify;’>
								<p class='tinggi'>Apabila antrian Saudara sudah memenuhi persyaratan untuk diberi izin penghunian Rumah Negara dan terdapat Rumah Negara yang kosong dan siap ditempati, Saudara akan dihubungi oleh pegawai Subbagian Kesejahteraan untuk memastikan kebersediaan Saudara menempati Rumah Negara tersebut</br></p>
					</li>
					<li style=‘text-align: justify;’>
						<p class='tinggi'>Saudara dapat memantau proses penerbitan SKEP pada menu&nbsp; <strong><font size=3><span class='label label-primary'>STATUS ANTRIAN</span></font> - <font size=3><span class='label label-primary'>ANTRIAN DISETUJUI</strong></span></font> atau <font size=3><span class='label label-primary'><strong>STATUS PERPANJANGAN</span> - <span class='label label-primary'> PERPANJANGAN DIPROSES. </span></strong></font></p>
					</li>
					<li style=‘text-align: justify;’>
						<p class='tinggi'>Jika status di menu tersebut menunjukkan<strong>&nbsp; <span style=‘color: #00ff00;’><strong>&nbsp <font size=3><span class='label label-success'>SUDAH TERBIT SKEP</span></font></strong></span></strong></span> , Saudara bisa men-download SKEP Surat Izin Penghunian di menu&nbsp; <strong><font size=3><span class='label label-primary'>STATUS ANTRIAN / PERPANJANGAN </span> - <span class='label label-primary'> DOWNLOAD SKEP. </span></strong></font></p>
					</li>
					<li style=‘text-align: justify;’>
						<p class='tinggi'><strong>PROSES SELESAI.</strong></p>
					</li>
				</ol>




	        			</div>
	        	</div>	
	        	</div>
	        	<div class='col-md-4'>
	        	<div class='panel panel-info'>
	        		<div class='panel-heading'><b>DOWNLOAD FILE</b></div>
	        			<div class='panel-body'>
<div style='text-align: center'>
<a href='".asset('public/antrian.docx')."' align='center' class='btn btn-primary' role='button'><strong>File Permohonan Antrian Rumah Negara</strong></a></br></br>
<a href='".asset('public/perpanjangan.docx')."' align='center' class='btn btn-success' role='button'><strong>File Permohonan Perpanjangan SIP</strong></a></br></br>
	        			</div></div>
	        	</div>
	        	</div>
	        	</div>
	        	";

	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	       $query->LIMIT(3);     
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 


	}